#import <Foundation/Foundation.h>


@interface NSString (Utils)

+ (NSString *) getCurrentTime;
- (NSString *) substituteEmoticons;

@end
