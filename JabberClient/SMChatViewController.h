//
//  SMChatViewController.h
//  JabberClient
//
//  Created by Developer on 6/22/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "XMPP.h"
#import "TURNSocket.h"
#import "SMMessageViewTableCell.h"
#import "SMMessageDelegate.h"
//#import "NSString+Utils.h"

@interface SMChatViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SMMessageDelegate> {
    
	UITableView		*tView;
	NSMutableArray	*messages;
    NSMutableArray *turnSockets;
	
}

@property (nonatomic,retain) IBOutlet UITextField *messageField;
@property (nonatomic,retain) NSString *chatWithUser;
@property (nonatomic,retain) IBOutlet UITableView *tView;
@property (nonatomic,retain) NSMutableArray *messages;
- (id) initWithUser:(NSString *) userName;
- (IBAction) sendMessage;
- (IBAction) closeChat;

@end
