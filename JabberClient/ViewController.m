//
//  ViewController.m
//  JabberClient
//
//  Created by Developer on 6/20/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import "ViewController.h"
#import "SMLoginViewController.h"
#import "XMPP.h"
#import "AppDelegate.h"
//#import "XMPPRoster.h"
@implementation ViewController
@synthesize tView;


-(void) CHAT{
    
    SMChatViewController *loginController = [[SMChatViewController alloc] init];
    [self presentModalViewController:loginController animated:YES];
}


- (AppDelegate *)appDelegate {
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (XMPPStream *)xmppStream {
    return [[self appDelegate] xmppStream];
} 
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tView.delegate = self;
    self.tView.dataSource = self;
    
    AppDelegate *del = [self appDelegate];
    del._chatDelegate = self;
    
    onlineBuddies = [[NSMutableArray alloc ] init];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *userName = (NSString *) [onlineBuddies objectAtIndex:indexPath.row];
    SMChatViewController *chatController = [[SMChatViewController alloc] initWithUser:userName];
    [self presentModalViewController:chatController animated:YES];
}

- (void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:animated];
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    if (login) {
        if ([[self appDelegate] connect]) {
            NSLog(@"show buddy list");
                        
        }
    } else {
        [self showLogin];
    }
}

- (void)newBuddyOnline:(NSString *)buddyName {
    [onlineBuddies addObject:buddyName];
    [self.tView reloadData];
}
- (void)buddyWentOffline:(NSString *)buddyName {
    [onlineBuddies removeObject:buddyName];
    [self.tView reloadData];
}

- (void) showLogin {
    SMLoginViewController *loginController = [[SMLoginViewController alloc] init];
    [self presentModalViewController:loginController animated:YES];
}
#pragma mark -
#pragma mark Table view delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *s = (NSString *) [onlineBuddies objectAtIndex:indexPath.row];
    static NSString *CellIdentifier = @"UserCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = s;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [onlineBuddies count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end