

#import <UIKit/UIKit.h>
#import "NSString+Utils.h"

@protocol SMMessageDelegate

- (void)newMessageReceived:(NSDictionary *)messageContent;

@end
