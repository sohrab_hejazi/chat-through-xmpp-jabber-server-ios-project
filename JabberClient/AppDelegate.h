//
//  AppDelegate.h
//  JabberClient
//
//  Created by Developer on 6/20/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XMPP.h"
//#import "XMPPRoster.h"
#import "SMChatDelegate.h"
#import "SMMessageDelegate.h"
//#import "NSString+Utils.h"

@class ViewController;
@interface AppDelegate : NSObject  {
    UIWindow *window;
    ViewController *viewController;
    XMPPStream *xmppStream;
    NSString *password;
    BOOL isOpen;
    __weak NSObject <SMChatDelegate> *chatDelegate;
	__weak NSObject <SMMessageDelegate> *messageDelegate;

}
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ViewController *viewController;
@property (nonatomic, readonly) XMPPStream *xmppStream;
@property (nonatomic, assign) id  _chatDelegate;
@property (nonatomic, assign) id  _messageDelegate;
- (BOOL)connect;
- (void)disconnect;
@end