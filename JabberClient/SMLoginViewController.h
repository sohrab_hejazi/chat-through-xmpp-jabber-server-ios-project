//
//  SMLoginViewController.h
//  JabberClient
//
//  Created by Developer on 6/22/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMLoginViewController : UIViewController {
    UITextField *loginField;
    UITextField *passwordField;
}
@property (nonatomic,retain) IBOutlet UITextField *loginField;
@property (nonatomic,retain) IBOutlet UITextField *passwordField;
- (IBAction) login;
- (IBAction) hideLogin;
@end