//
//  ViewController.h
//  JabberClient
//
//  Created by Developer on 6/20/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JabberClientAppDelegate.h"
#import "SMLoginViewController.h"
#import "SMChatViewController.h"
#import "SMChatDelegate.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SMChatDelegate> {

    UITableView *tView;
    NSMutableArray *onlineBuddies;
}
@property (nonatomic,retain) IBOutlet UITableView *tView;
- (IBAction) showLogin;
- (IBAction) CHAT;
@end