//
//  SMLoginViewController.m
//  JabberClient
//
//  Created by Developer on 6/22/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import "SMLoginViewController.h"

@interface SMLoginViewController ()

@end

@implementation SMLoginViewController
@synthesize loginField, passwordField;
- (IBAction) login {
    [[NSUserDefaults standardUserDefaults] setObject:self.loginField.text forKey:@"userID"];
    [[NSUserDefaults standardUserDefaults] setObject:self.passwordField.text forKey:@"userPassword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissModalViewControllerAnimated:YES];
}
- (IBAction) hideLogin {
    [self dismissModalViewControllerAnimated:YES];
}
@end